
CC := arm-linux-androideabi-gcc

BIN := injectso
LIB := libtest.so

all: $(BIN) $(LIB)

$(BIN): injectso.c
	$(CC) -o $@ $<

$(LIB): libtest.c
	$(CC) -shared -o $@ $< -llog

